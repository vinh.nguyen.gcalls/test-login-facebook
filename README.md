# Test Login Facebook

This is a basic guide to help you run the "Test Login Facebook" project using Puppeteer on both Windows and Ubuntu environments.

## Requirements

To run this project, you need to have Node.js installed on your computer. You can download and install Node.js from the official Node.js website: [https://nodejs.org/](https://nodejs.org/). Additionally, you can choose to use either Yarn or npm as the package manager.

- To install Yarn, please follow the instructions provided at [https://yarnpkg.com/](https://yarnpkg.com/).
- To install npm, it is automatically installed with Node.js.

## Installation

After installing Node.js and the package manager of your choice (Yarn or npm), follow these steps to install the "Test Login Facebook" project:

1. Clone the source code from the Git repository:

   ```bash
   git clone https://gitlab.com/vinh.nguyen.gcalls/test-login-facebook.git
   ```

2. Navigate to the project directory:

   ```bash
   cd test-login-facebook
   ```

3. Install the dependencies using Yarn:

   ```bash
   yarn install
   ```

   Or, if you prefer to use npm:

   ```bash
   npm install
   ```

## Running the Project on Windows

To run the project on Windows, follow these steps:

1. Open Command Prompt or PowerShell.

2. Navigate to the project directory:

   ```bash
   cd test-login-facebook
   ```

3. Run the project using Yarn:

   ```bash
   yarn test
   ```

   Or, if you prefer to use npm:

   ```bash
   npm test
   ```

The above commands will run the project's source code and display the results on the console.

## Running the Project on Ubuntu

To run the project on Ubuntu, follow these steps:

1. Open Terminal.

2. Navigate to the project directory:

   ```bash
   cd test-login-facebook
   ```

3. Run the project using Yarn:

   ```bash
   yarn test
   ```

   Or, if you prefer to use npm:

   ```bash
   npm test
   ```

The above commands will run the project's source code and display the results on the console.

## Customization

You can customize the "Test Login Facebook" project according to your needs. The relevant project files are typically located in the `src` or `test` directory. You can edit these files to perform various tasks using Puppeteer.

## Support

If you encounter any issues or have questions related to the "Test Login Facebook" project, please create an issue in the Git repository or contact the development team.
