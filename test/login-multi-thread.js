const { Cluster } = require('puppeteer-cluster');
const { expect } = require('chai');
const fs = require('fs');
const { Browser } = require('puppeteer');

const data = fs.readFileSync('./test/input/input.txt', 'utf8');
const accounts = data.split('\r\n');

const accountObjects = accounts.map((account) => {
    const [uid, password, verify, cookie, mail, passmail] = account.split('|');
    return { uid, password, verify, cookie, mail, passmail };
});

(async () => {
    const cluster = await Cluster.launch({
        concurrency: Cluster.CONCURRENCY_CONTEXT,
        maxConcurrency: 12,
        puppeteerOptions: {
            headless: false,
            slowMo: 10,
            devtools: false,
            defaultViewport: null,
        },
    });

    await cluster.task(async ({ page, data }) => {
        const account = await data;
        await page.goto('https://www.facebook.com/');
        await page.type('#email', account.uid);
        await page.type('#pass', account.password);
        await page.click("button[name='login']");
        let currentURL = await page.url();
        let title;
        const content = `${account.uid}|${account.password}|${account.verify}|${account.cookie}|${account.mail}|${account.passmail}\n`;

        if (currentURL.startsWith('https://www.facebook.com/login/device-based/')) {
            // console.log('account invalid', currentURL);
            if (!fs.existsSync('./test/output/invalid.txt')) {
                fs.writeFileSync('./test/output/invalid.txt', content);
                console.log('The content has been written to the invalid.txt file.');
            } else {
                const fileContent = fs.readFileSync('./test/output/invalid.txt', 'utf8');
                if (!fileContent.includes(content)) {
                    fs.appendFileSync('./test/output/invalid.txt', content);
                    console.log('The content has been written to the invalid.txt file.');
                } else {
                    console.log('The data already exists in the invalid.txt file. Do not add.');
                }
            }
            title = await page.title();
            expect(title).to.equal("Facebook");
        }
        else if (currentURL === 'https://www.facebook.com/') {
            if (!fs.existsSync('./test/output/valid.txt')) {
                fs.writeFileSync('./test/output/valid.txt', content);
                console.log('The content has been written to the valid.txt file.');
            } else {
                const fileContent = fs.readFileSync('./test/output/valid.txt', 'utf8');
                if (!fileContent.includes(content)) {
                    fs.appendFileSync('./test/output/valid.txt', content);
                    console.log('The content has been written to the valid.txt file.');
                } else {
                    console.log('The data already exists in the valid.txt file. Do not add.');
                }
            }
            title = await page.title();
            expect(title).to.match(/^.+Facebook$/);
        }
        else {
            const response = await fetch(`https://2fa.live/tok/${account.verify}`);
            const token = await response.json();
            await page.type('#approvals_code', token.token);
            await page.click("#checkpointSubmitButton");
            await page.click("input[value='dont_save']");
            await page.click("#checkpointSubmitButton");
            const currentURL = await page.url();

            if (currentURL.startsWith('https://www.facebook.com/checkpoint/?next')) {
                if (!fs.existsSync('./test/output/review.txt')) {
                    fs.writeFileSync('./test/output/review.txt', content);
                    console.log('The content has been written to the file review.txt.');
                } else {
                    const fileContent = fs.readFileSync('./test/output/review.txt', 'utf8');
                    if (!fileContent.includes(content)) {
                        fs.appendFileSync('./test/output/review.txt', content);
                        console.log('The content has been written to the file review.txt.');
                    } else {
                        console.log('The data already exists in the file review.txt. Do not add.');
                    }
                }
                title = await page.title();
                expect(title).to.equal("Facebook");
            } else if (currentURL === 'https://www.facebook.com/') {
                if (!fs.existsSync('./test/output/valid.txt')) {
                    fs.writeFileSync('./test/output/valid.txt', content);
                    console.log('The content has been written to the valid.txt file.');
                } else {
                    const fileContent = fs.readFileSync('./test/output/valid.txt', 'utf8');
                    if (!fileContent.includes(content)) {
                        fs.appendFileSync('./test/output/valid.txt', content);
                        console.log('The content has been written to the valid.txt file.');
                    } else {
                        console.log('The data already exists in the valid.txt file. Do not add.');
                    }
                }
                title = await page.title();
                expect(title).to.match(/^.+Facebook$/);
            }
        }

    });

    for (let index in accountObjects) {
        await cluster.queue(accountObjects[index]);
    }

    await cluster.idle();
    await cluster.close();
})();
