const puppeteer = require('puppeteer');
const { expect } = require('chai');
const fs = require('fs');

const data = fs.readFileSync('./test/input/input.txt', 'utf8');
// split data to array from file input.txt
const accounts = data.split('\r\n');

// save username and password to array of object
const accountObjects = accounts.map((account) => {
    const [uid, password, verify, cookie, mail, passmail] = account.split('|');
    return { uid, password, verify, cookie, mail, passmail };
}
);
for (let index in accountObjects) {
    async function log2FA() {
        const response = await fetch(`https://2fa.live/tok/${accountObjects[index].verify}`);
        const token = await response.json();
        return token.token;
    }

    describe('Test login page Facebook', async () => {
        let browser;
        let page;
        let title;

        before(async () => {
            browser = await puppeteer.launch({
                headless: false,
                slowMo: 10,
                devtools: false,
                defaultViewport: null,
            });
            page = await browser.newPage();
        }
        );

        beforeEach(async () => {
            // Before each test, open a new page.
            // page = await browser.newPage();
            await page.goto('https://www.facebook.com/');
        }
        );

        const account = accountObjects[index];
        const content = `${account.uid}|${account.password}|${account.verify}|${account.cookie}|${account.mail}|${account.passmail}\n`;


        it('Login successfully', async () => {
            await page.type('#email', account.uid);
            await page.type('#pass', account.password);
            await page.click("button[name='login']");
            const currentURL = await page.url();
            if (currentURL.startsWith('https://www.facebook.com/login/')) {
                if (!fs.existsSync('./test/output/invalid.txt')) {
                    fs.writeFileSync('./test/output/invalid.txt', content);
                    console.log('The content has been written to the invalid.txt file.');
                } else {
                    const fileContent = fs.readFileSync('./test/output/invalid.txt', 'utf8');
                    if (!fileContent.includes(content)) {
                        fs.appendFileSync('./test/output/invalid.txt', content);
                        console.log('The content has been written to the invalid.txt file.');
                    } else {
                        console.log('The data already exists in the invalid.txt file. Do not add.');
                    }
                }
                title = await page.title();
                expect(title).to.equal("Facebook");
            }
            else if (currentURL === 'https://www.facebook.com/') {
                if (!fs.existsSync('./test/output/valid.txt')) {
                    fs.writeFileSync('./test/output/valid.txt', content);
                    console.log('The content has been written to the valid.txt file.');
                } else {
                    const fileContent = fs.readFileSync('./test/output/valid.txt', 'utf8');
                    if (!fileContent.includes(content)) {
                        fs.appendFileSync('./test/output/valid.txt', content);
                        console.log('The content has been written to the valid.txt file.');
                    } else {
                        console.log('The data already exists in the valid.txt file. Do not add.');
                    }
                }
                await page.waitForTimeout(1000);
                title = await page.title();
                console.log("Title: ", title)
                expect(title).to.match(/^.+Facebook$/);
            }
            else {
                await page.type('#approvals_code', await log2FA());
                await page.click("#checkpointSubmitButton");
                await page.click("input[value='dont_save']");
                await page.click("#checkpointSubmitButton");
                // await page.waitForTimeout(5000);
                // await page.waitForNavigation();
                const currentURL = await page.url();

                if (currentURL.startsWith('https://www.facebook.com/checkpoint/?next')) {
                    if (!fs.existsSync('./test/output/review.txt')) {
                        fs.writeFileSync('./test/output/review.txt', content);
                        console.log('The content has been written to the file review.txt.');
                    } else {
                        const fileContent = fs.readFileSync('./test/output/review.txt', 'utf8');
                        if (!fileContent.includes(content)) {
                            fs.appendFileSync('./test/output/review.txt', content);
                            console.log('The content has been written to the file review.txt.');
                        } else {
                            console.log('The data already exists in the file review.txt. Do not add.');
                        }
                    }
                    title = await page.title();
                    expect(title).to.equal("Facebook");
                } else if (currentURL === 'https://www.facebook.com/') {
                    if (!fs.existsSync('./test/output/valid.txt')) {
                        fs.writeFileSync('./test/output/valid.txt', content);
                        console.log('The content has been written to the valid.txt file.');
                    } else {
                        const fileContent = fs.readFileSync('./test/output/valid.txt', 'utf8');
                        if (!fileContent.includes(content)) {
                            fs.appendFileSync('./test/output/valid.txt', content);
                            console.log('The content has been written to the valid.txt file.');
                        } else {
                            console.log('The data already exists in the valid.txt file. Do not add.');
                        }
                    }
                    title = await page.title();
                    expect(title).to.match(/^.+Facebook$/);
                }
            }
        }
        );

        afterEach(async () => {
            await page.close();
        }
        );

        after(async () => {
            await browser.close();
        }
        );
    });
}
